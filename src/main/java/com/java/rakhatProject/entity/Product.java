package com.java.rakhatProject.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product")
public class Product {
    @Id
    @Column(name = "id")
    private long productId;
 /*   @Column(name = "image")
    private byte image;*/
    private String name;
    private String description;
    private double weight;
    private String unit;
    private String composition;
    @Column(name = "category_id")
    private long categoryId;
    private float price;
    private long stock;

}

package com.java.rakhatProject.repository;

import com.java.rakhatProject.entity.Auth;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;


@Repository
public interface AuthRepository extends CrudRepository<Auth, Long> {
    Auth findByUsernameAndPassword(String username, String password);
    Auth findByToken(String token);

    @Transactional
    @Modifying
    @Query(
            value = "insert into auth (username, password) values ( :username,:password)",
            nativeQuery = true)
    void createRegister(@Param("username") String username,
                       @Param("password") String password);
}

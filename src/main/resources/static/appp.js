var app = angular.module('rakhat-project', []);
app.controller('RegisterCtrl', function($scope, $http) {

    $scope.registration = {
        username: '',
        password: ''
    }

    let register = function (registration) {
        $http({
            url: 'http://127.0.0.1:5000/registration',
            method: "POST",
            headers: {
                "Access-Control-Allow-Origin": "",
                "Content-Type": "application/json",
            },
            data: {
                'username' : registration.username,
                'password' : registration.password,
            }
        })
            .then(function (response) {
                    $scope.registration = response.data;
                },
                function (response) { //optional
                    $scope.registration = {}
                });
    };

    $scope.registerUser = function (registration) {
        $http({
            url: 'http://127.0.0.1:5000/registration',
            method: "POST",
            headers: {
                "Access-Control-Allow-Origin": "",
                "Content-Type": "application/json",
            },
            data: {
                'username' : registration.username,
                'password' : registration.password,
            }
        })
            .then(function (response) {
                    $scope.registration = response.data;
                    register(registration);
                },
                function (response) { //optional
                    $scope.registration = {}
                });
    };
});